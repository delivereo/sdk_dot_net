﻿Delivereo.SDK

Installation
==========================================================

Release 1.0.16
==========================================================

Added new cities

Release 1.0.15
==========================================================

Added new payment modes, CHECK, AGREEMENT and MIXED

Release 1.0.14
==========================================================

Added new booking status state, MANUAL_ASSIGN

Release 1.0.13
==========================================================

New optional fields client phone number and client address reference

Release 1.0.12
==========================================================

New payment mode NONE for domicile and domicile pharmacy API

Release 1.0.11
==========================================================

Fix issue with SSH domains

Release 1.0.10
==========================================================

Fix issue with null reference exception when user does not exists in the environment
while trying to login

Release 1.0.9
==========================================================

Added method to create booking domicile and domicile pharmacy

Delivereo SDK allows you to connect to the Delivereo Business API easily

Release 1.0.8
==========================================================

Added logic to send address as one field instead of using main and crossing street
Modified validate address service to accept city

Release 1.0.7
==========================================================

Added validate address service


Release 1.0.6
==========================================================

Removed Flurl dependency

Example
==========================================================

using Delivereo.SDK.Base;
using Delivereo.SDK.Client;
using Delivereo.SDK.Enums;
using Delivereo.SDK.Request.Calculate;
using Delivereo.SDK.Request.Create;
using Delivereo.SDK.Response.Create;
using Delivereo.SDK.Response.Login;
using System;
using System.Collections.Generic;

namespace Delivereo.Client
{
    public class Program
    {
        private static string jwtToken;
        private static int bookingId;
        private static int bookingIdToCancel;

        static void Main(string[] args)
        {
            var businessClient = new BusinessClient(ServerEnvironment.Staging, Language.Spanish);

            try
            {
                Login(businessClient);
                ValidateOneAddress(businessClient);
                //RenewToken(businessClient);
                CalculateBookingWithPoints(businessClient);
                CalculateBookingWithAddresses(businessClient);
                CreateBookingWithPoints(businessClient);
                CreateBookingWithAddresses(businessClient);
                CreateBookingDomicile(businessClient);
                CreateBookingDomicilePharmacy(businessClient);
                CancelBooking(businessClient);
                RetryBooking(businessClient);
                RateBooking(businessClient);
                DetailBooking(businessClient);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }

        private static void Login(BusinessClient businessClient)
        {
            Console.WriteLine("Sending login request.");
            var loginResponse = businessClient.Login("69982032-DD5", "info@delivereo.com", "1792715083001");
            ProcessResponse(loginResponse, "Login request successful.");
        }

        private static void RenewToken(BusinessClient businessClient)
        {
            Console.WriteLine("Sending login request.");
            var renewTokenResponse = businessClient.TokenRenewal(jwtToken, "info@delivereo.com");
            ProcessResponse(renewTokenResponse, "Login request successful.");
        }

        private static void ValidateOneAddress(BusinessClient businessClient)
        {
            Console.WriteLine("Sending validate addresses request.");
            var validateAddressResponse = businessClient.ValidateBookingAddress(jwtToken, City.QUITO, "Amazonas y Veintimilla", "", "", "EC");
            ProcessResponse(validateAddressResponse, "Validate addresses request successful.");
        }

        private static void CalculateBookingWithPoints(BusinessClient businessClient)
        {
            var points = new List<CalculatePoint>()
                {
                    new CalculatePoint()
                    {
                        PointOrder = 1,
                        PointLatitude = -0.19045013,
                        PointLongitude = -78.48059853
                    },
                      new CalculatePoint()
                    {
                        PointOrder = 2,
                        PointLatitude = -0.172213,
                        PointLongitude = -78.486467
                    },
                        new CalculatePoint()
                    {
                        PointOrder = 3,
                        PointLatitude = -0.20963146,
                        PointLongitude = -78.43948563
                    }
                };

            Console.WriteLine("Sending calculate with points request.");
            var calculateResponse = businessClient.CalculateBookingCostUsingPoints(jwtToken, City.QUITO, points);
            ProcessResponse(calculateResponse, "Calculate with points request successful.");
        }

        private static void CalculateBookingWithAddresses(BusinessClient businessClient)
        {
            var addresses = new List<CalculateAddress>()
                {
                    new CalculateAddress()
                    {
                        AddressOrder = 1,
                        FullAddress = "Amazonas y Veintimilla",
                        AddressMainStreet = "",
                        AddressCrossingStreet = "",
                        CountryCode = "EC"
                    },
                      new CalculateAddress()
                    {
                        AddressOrder = 2,
                        FullAddress = "Francisco Dalmau y Calle 1",
                        AddressMainStreet = "Francisco Dalmau",
                        AddressCrossingStreet = "Calle 1",
                        CountryCode = "EC"
                    }
                };

            Console.WriteLine("Sending calculate with addresses request.");
            var calculateAddressResponse = businessClient.CalculateBookingCostUsingAddresses(jwtToken, City.QUITO, addresses);
            ProcessResponse(calculateAddressResponse, "Calculate with addresses request successful.");
        }

        private static void CreateBookingWithPoints(BusinessClient businessClient)
        {
            var points = new List<CreatePoint>()
                {
                    new CreatePoint()
                    {
                        PointOrder = 1,
                        PointLatitude = -0.19045013,
                        PointLongitude = -78.48059853,
                        SenderRecipientName = "Jorge Cardenas",
                        Address = "Amazonas y Veintimilla E32A-32",
                        Reference = "Edificio Tez Apartamento 200"
                    },
                    new CreatePoint()
                    {
                        PointOrder = 2,
                        PointLatitude = -0.172213,
                        PointLongitude = -78.486467,
                        SenderRecipientName = "Marcelo Bonilla",
                        Address = "Diego de Vazquez y Bellavista E34-23",
                        Reference = "Condominio Carmen 2 casa 34"
                    },
                    new CreatePoint()
                    {
                        PointOrder = 3,
                        PointLatitude = -0.20963146,
                        PointLongitude = -78.43948563,
                        SenderRecipientName = "Pablo Mancero",
                        Address = "Naciones Unidas e Inaquito Esquina",
                        Reference = "Edificio Metropolitan Oficina 409"
                    }
                };

            Console.WriteLine("Sending create with points request.");
            var createResponse = businessClient.CreateBookingUsingPoints(jwtToken, City.QUITO, points,
                "Compra hamburguesa de pollo y hamburguesa de carne", 11.2, new CreateOrder()
                {
                    OrderGuid = "123456",
                    OrderItems = new List<CreateOrderItem>(),
                    OrderSubTotal = 10,
                    OrderIva = 1.2,
                    OrderTotal = 11.2,
                    PaymentMode = PaymentMode.CREDIT_CARD
                }, bookingBusinessUserEmail: "");

            ProcessResponse(createResponse, "Create with points request successful.");
        }

        private static void CreateBookingWithAddresses(BusinessClient businessClient)
        {
            var addresses = new List<CreateAddress>()
                {
                    new CreateAddress()
                    {
                        AddressOrder = 1,
                        FullAddress = "Amazonas y Veintimilla",
                        AddressMainStreet = "",
                        AddressCrossingStreet = "",
                        CountryCode = "EC",
                        SenderRecipientName = "Jorge Cardenas",
                        Address = "Amazonas y Veintimilla E32A-32",
                        Reference = "Edificio Tez Apartamento 200"
                    },
                      new CreateAddress()
                    {
                        AddressOrder = 2,
                        FullAddress = "Francisco Dalmau y Calle 1",
                        AddressMainStreet = "",
                        AddressCrossingStreet = "",
                        CountryCode = "EC",
                        SenderRecipientName = "Daniel Mancero",
                        Address = "Francisco Dalmau y Calle 1 OE32",
                        Reference = "Frente al Supermaxi"
                    }
                };

            Console.WriteLine("Sending create with addresses request.");
            var createAddressResponse = businessClient.CreateBookingUsingAddresses(jwtToken, City.QUITO, addresses,
                "Compra hamburguesa de pollo y hamburguesa de carne", 11.2, new CreateOrder()
                {
                    OrderGuid = "123456",
                    OrderItems = new List<CreateOrderItem>(),
                    OrderSubTotal = 10,
                    OrderIva = 1.2,
                    OrderTotal = 11.2,
                    PaymentMode = PaymentMode.CREDIT_CARD
                }, bookingBusinessUserEmail: "");

            ProcessResponse(createAddressResponse, "Create with addresses request successful.");
        }

        private static void CreateBookingDomicile(BusinessClient businessClient)
        {
            Console.WriteLine("Sending create booking domicile request.");
            var createBookingDomicileResponse = businessClient.CreateBookingDomicilePharmacy(jwtToken, "CCI Quito",
                new List<string>(), "stefano@delivereo.com", "12345", "1715245933", "Daniel Mancero",
                "Amazonas y naciones unidas", PaymentMode.CREDIT_CARD, "EC", "+593-983273919", "Departamento 5");

            ProcessResponse(createBookingDomicileResponse, "Create booking domicile request successful.");
        }

        private static void CreateBookingDomicilePharmacy(BusinessClient businessClient)
        {
            Console.WriteLine("Sending create booking domicile pharmacy request.");
            var createBookingDomicilePharmacyResponse = businessClient.CreateBookingDomicilePharmacy(jwtToken, "CCI Quito",
                new List<string>() { "Inca Quito" }, "stefano@delivereo.com", "12345", "1715245933", "Daniel Mancero",
                "Amazonas y naciones unidas", PaymentMode.CREDIT_CARD, "EC", "+593-983273919", "Departamento 5");

            ProcessResponse(createBookingDomicilePharmacyResponse, "Create booking domicile pharmacy request successful.");
        }

        private static void RateBooking(BusinessClient businessClient)
        {
            Console.WriteLine("Sending rate request.");
            var rateResponse = businessClient.RateDriverForBookingById(jwtToken, bookingId, 5.0, RatingLowReason.NONE, "Everything OK");
            ProcessResponse(rateResponse, "Rate request successful.");
        }

        private static void RetryBooking(BusinessClient businessClient)
        {
            Console.WriteLine("Sending retry request.");
            var retryResponse = businessClient.RetryBookingById(jwtToken, bookingId);
            ProcessResponse(retryResponse, "Retry request successful.");
        }

        private static void CancelBooking(BusinessClient businessClient)
        {
            Console.WriteLine("Sending cancel request.");
            var cancelResponse = businessClient.CancelBookingById(jwtToken, bookingIdToCancel, CancelReason.NOT_NEEDED_ANYMORE, "Another option was found");
            ProcessResponse(cancelResponse, "Cancel request successful.");
        }

        private static void DetailBooking(BusinessClient businessClient)
        {
            Console.WriteLine("Sending detail request.");
            var detailResponse = businessClient.GetBookingFullDetailById(jwtToken, bookingId);
            ProcessResponse(detailResponse, "Detail request successful.");
        }

        private static void ProcessResponse<T>(T response, string successMessage) where T : BasicResponse
        {
            if (response != null && response.Status)
            {
                Console.WriteLine(response.Message);
                Console.WriteLine(successMessage);

                if (response is SignInResponse)
                {
                    jwtToken = (response as SignInResponse).JwtToken;
                }

                if (response is CreateResponse)
                {
                    bookingId = (response as CreateResponse).BookingId;
                    bookingIdToCancel = (response as CreateResponse).BookingId;
                }
            }
            else
            {
                throw new Exception(response?.Message ?? "Error");
            }

            Console.ReadKey();
        }
    }
}