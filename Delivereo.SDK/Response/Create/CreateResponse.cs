﻿using Delivereo.SDK.Base;

namespace Delivereo.SDK.Response.Create
{
    /// <summary>
    /// Create response
    /// </summary>
    public class CreateResponse : BasicResponse
    {
        public int BookingId { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateResponse()
        {
            // Do nothing
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        public CreateResponse(BasicResponse basicResponse)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;
        }

        /// <summary>
        /// Complete constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        /// <param name="orderId">Booking id</param>
        public CreateResponse(BasicResponse basicResponse, int bookingId)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;

            this.BookingId = bookingId;
        }
    }
}