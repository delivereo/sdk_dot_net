﻿using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;
using System;
using System.Collections.Generic;

namespace Delivereo.SDK.Detail.Response
{
    /// <summary>
    /// Booking response
    /// </summary>
    public class BookingResponse : BasicResponse
    {
        public int BookingId { get; set; }

        public long UserId { get; set; }

        public long GeneralUserId { get; set; }

        public long? DriverId { get; set; }

        public string DriverFirstName { get; set; }

        public string DriverLastName { get; set; }

        public string DriverEmail { get; set; }

        public string DriverMobileNumber { get; set; }

        public string DriverRegistrationNumber { get; set; }

        public TransportMode DriverTransport { get; set; }

        public string DriverTransportModeName { get; set; }

        public string FirstPointSender { get; set; }

        public string FirstPointAddress { get; set; }

        public DateTime FirstPointInitialTime { get; set; }

        public DateTime FirstPointArrivalTime { get; set; }

        public string LastPointRecipient { get; set; }

        public string LastPointAddress { get; set; }

        public DateTime LastPointInitialTime { get; set; }

        public DateTime LastPointArrivalTime { get; set; }

        public double Fare { get; set; }

        public string Description { get; set; }

        public bool ReceiptStatus { get; set; }

        public BookingStatus BookingStatus { get; set; }

        public string BookingStatusName { get; set; }

        public double ItemsPrice { get; set; }

        public double InsuredAmount { get; set; }

        public double Iva { get; set; }

        public double TotalAmount { get; set; }

        public string StoreName { get; set; }

        public string OtherImage { get; set; }
        
        public string TotalDuration { get; set; }

        public string ExtraInstructions { get; set; }

        public long? CurrentExtraPointId { get; set; }

        public string PublicGuid { get; set; }

        public string PublicUrl { get; set; }

        public List<BookingPointResponse> ExtraPoints { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public BookingResponse()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        public BookingResponse(BasicResponse basicResponse)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;
        }

        /// <summary>
        /// Complete constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        /// <param name="bookingId">Booking id</param>
        /// <param name="userId">Business user id, api user</param>
        /// <param name="generalUserId">General user id</param>
        /// <param name="driverId">Driver id</param>
        /// <param name="driverFirstName">Driver first name</param>
        /// <param name="driverLastName">Driver last name</param>
        /// <param name="driverEmail">driver email</param>
        /// <param name="driverMobileNumber">Driver mobile number</param>
        /// <param name="driverRegistrationNumber">Driver registration number</param>
        /// <param name="driverTransport">Driver transport mode</param>
        /// <param name="driverTransportModeName">Driver transport mode name</param>
        /// <param name="firstPointSender">First point sender name</param>
        /// <param name="firstPointAddress">First point sender address</param>
        /// <param name="firstPointInitialTime">First point initial time</param>
        /// <param name="firstPointArrivalTime">First point arrival time</param>
        /// <param name="lastPointRecipient">Last point recipient name</param>
        /// <param name="lastPointAddress">Last point recipient address</param>
        /// <param name="lastPointInitialTime">Last point initial time</param>
        /// <param name="lastPointArrivalTime">Last point arrival time</param>
        /// <param name="fare">Fare price</param>
        /// <param name="description">Booking description</param>
        /// <param name="bookingStatus">Booking status</param>
        /// <param name="bookingStatusName">Booking status name</param>
        /// <param name="itemsPrice">Booking items total amount without taxes</param>
        /// <param name="insuredAmount">Booking insuread amount</param>
        /// <param name="iva">Iva percentage</param>
        /// <param name="totalAmount">Booking total amount with taxes and fare</param>
        /// <param name="storeName">Store name if provided</param>
        /// <param name="otherImage">Other image from the driver</param>
        /// <param name="totalDuration">Total duration of the booking</param>
        /// <param name="extraInstructions">Extra instructions of the booking</param>
        /// <param name="currentExtraPointId">Current extra point</param>
        /// <param name="publicGuid">Public guid of the booking</param>
        /// <param name="publicUrl">Public url of the booking</param>
        /// <param name="extraPoints">Extra points of the booking</param>
        public BookingResponse(BasicResponse basicResponse, int bookingId, long userId, long generalUserId, long? driverId, string driverFirstName, string driverLastName, string driverEmail, string driverMobileNumber, string driverRegistrationNumber, TransportMode driverTransport, string driverTransportModeName, string firstPointSender, string firstPointAddress, DateTime firstPointInitialTime, DateTime firstPointArrivalTime, string lastPointRecipient, string lastPointAddress, DateTime lastPointInitialTime, DateTime lastPointArrivalTime, double fare, string description, BookingStatus bookingStatus, string bookingStatusName, double itemsPrice, double insuredAmount, double iva, double totalAmount, string storeName, string otherImage, string totalDuration, string extraInstructions, long? currentExtraPointId, string publicGuid, string publicUrl, List<BookingPointResponse> extraPoints)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;

            this.BookingId = bookingId;
            this.UserId = userId;
            this.GeneralUserId = generalUserId;
            this.DriverId = driverId;
            this.DriverFirstName = driverFirstName;
            this.DriverLastName = driverLastName;
            this.DriverEmail = driverEmail;
            this.DriverMobileNumber = driverMobileNumber;
            this.DriverRegistrationNumber = driverRegistrationNumber;
            this.DriverTransport = driverTransport;
            this.DriverTransportModeName = driverTransportModeName;
            this.FirstPointSender = firstPointSender;
            this.FirstPointAddress = firstPointAddress;
            this.FirstPointInitialTime = firstPointInitialTime;
            this.FirstPointArrivalTime = firstPointArrivalTime;
            this.LastPointRecipient = lastPointRecipient;
            this.LastPointAddress = lastPointAddress;
            this.LastPointInitialTime = lastPointInitialTime;
            this.LastPointArrivalTime = lastPointArrivalTime;
            this.Fare = fare;
            this.Description = description;
            this.BookingStatus = bookingStatus;
            this.BookingStatusName = bookingStatusName;
            this.ItemsPrice = itemsPrice;
            this.InsuredAmount = insuredAmount;
            this.Iva = iva;
            this.TotalAmount = totalAmount;
            this.StoreName = storeName;
            this.OtherImage = otherImage;
            this.TotalDuration = totalDuration;
            this.ExtraInstructions = extraInstructions;
            this.CurrentExtraPointId = currentExtraPointId;
            this.PublicGuid = publicGuid;
            this.PublicUrl = publicUrl;
            this.ExtraPoints = extraPoints;
        }
    }
}