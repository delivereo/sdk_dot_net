﻿using System;

namespace Delivereo.SDK.Detail.Response
{
    /// <summary>
    /// Booking point response
    /// </summary>
    public class BookingPointResponse
    {
        public int PointId { get; set; }

        public int PointOrder { get; set; }

        public string SenderRecipientName { get; set; }

        public string Address { get; set; }

        public string Reference { get; set; }

        public DateTime PointInitialTime { get; set; }

        public DateTime PointArrivalTime { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public BookingPointResponse()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="pointId">Point id</param>
        /// <param name="pointOrder">Point order</param>
        /// <param name="senderRecipientName">Sender or recipient name</param>
        /// <param name="address">Real full address</param>
        /// <param name="reference">Reference to the full address</param>  
        /// <param name="pointInitialTime">Point initial time</param>
        /// <param name="pointArrivalTime">Point arrival time</param>
        public BookingPointResponse(int pointId, int pointOrder, string senderRecipientName, string address, string reference, DateTime pointInitialTime, DateTime pointArrivalTime)
        {
            this.PointId = pointId;
            this.PointOrder = pointOrder;
            this.SenderRecipientName = senderRecipientName;
            this.Address = address;
            this.Reference = reference;
            this.PointInitialTime = pointInitialTime;
            this.PointArrivalTime = pointArrivalTime;
        }
    }
}