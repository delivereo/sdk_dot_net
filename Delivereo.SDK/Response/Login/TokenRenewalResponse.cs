﻿
using Delivereo.SDK.Base;

namespace Delivereo.SDK.Response.Login
{
    /// <summary>
    /// Token renewal response
    /// </summary>
    public class TokenRenewalResponse : BasicResponse
    {
        public string Result { get; set; }
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public TokenRenewalResponse()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        public TokenRenewalResponse(BasicResponse basicResponse)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;
        }

        /// <summary>
        /// Complete constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        /// <param name="result">Result response</param>
        public TokenRenewalResponse(BasicResponse basicResponse, string result)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;

            this.Result = result;
        }
    }
}