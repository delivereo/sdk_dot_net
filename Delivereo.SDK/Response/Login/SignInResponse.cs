﻿
using Delivereo.SDK.Base;

namespace Delivereo.SDK.Response.Login
{
    /// <summary>
    /// Sign in response
    /// </summary>
    public class SignInResponse : BasicResponse
    {
        public int Id { get; set; }

        public string Ruc { get; set; }

        public string BusinessName { get; set; }

        public string Email { get; set; }

        public string CountryCode { get; set; }

        public string MobileNumber { get; set; }

        public string BusinessImage { get; set; }

        public double? WalletBalance { get; set; }

        public string JwtToken { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SignInResponse()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        public SignInResponse(BasicResponse basicResponse)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;
        }

        /// <summary>
        /// Complete constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        /// <param name="id">Business id</param>
        /// <param name="ruc">Business ruc</param>
        /// <param name="businessName">Business name</param>
        /// <param name="email">Business email</param>
        /// <param name="countryCode">Business country code</param>
        /// <param name="mobileNumber">Business mobile number</param>
        /// <param name="businessImage">Business image url</param>
        /// <param name="walletBalance">Business wallet balance</param>
        /// <param name="jwtToken">Business JWT Token</param>
        public SignInResponse(BasicResponse basicResponse, int id, string ruc, string businessName, string email, string countryCode, string mobileNumber, string businessImage, double? walletBalance, string jwtToken)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;

            this.Id = id;
            this.Ruc = ruc;
            this.BusinessName = businessName;
            this.Email = email;
            this.CountryCode = countryCode;
            this.MobileNumber = mobileNumber;
            this.BusinessImage = businessImage;
            this.WalletBalance = walletBalance;
            this.JwtToken = jwtToken;
        }
    }
}