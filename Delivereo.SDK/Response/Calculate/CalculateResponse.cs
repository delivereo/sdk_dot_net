﻿using Delivereo.SDK.Base;

namespace Delivereo.SDK.Response.Calculate
{
    /// <summary>
    /// Calculate response
    /// </summary>
    public class CalculateResponse : BasicResponse
    {
        public double FarePrice { get; set; }

        public double ItemsPrice { get; set; }

        public double IvaPercentage { get; set; }

        public double Iva { get; set; }

        public double TotalAmount { get; set; }

        public string EstimatedTime { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CalculateResponse()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        public CalculateResponse(BasicResponse basicResponse)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;
        }

        /// <summary>
        /// Complete constructor
        /// </summary>
        /// <param name="basicResponse">Basic response</param>
        /// <param name="farePrice">Fare price</param>
        /// <param name="itemsPrice">Booking items total amount without taxes</param>
        /// <param name="ivaPercentage">Iva percentage</param>
        /// <param name="iva">Iva as value</param>
        /// <param name="totalAmount">Total amount to pay</param>
        /// <param name="estimatedTime">Estimated time</param>
        public CalculateResponse(BasicResponse basicResponse, double farePrice, double itemsPrice, double ivaPercentage, double iva, double totalAmount, string estimatedTime)
        {
            this.Title = basicResponse.Title;
            this.Message = basicResponse.Message;
            this.Status = basicResponse.Status;
            this.Code = basicResponse.Code;

            this.FarePrice = farePrice;
            this.ItemsPrice = itemsPrice;
            this.IvaPercentage = ivaPercentage;
            this.Iva = iva;
            this.TotalAmount = totalAmount;
            this.EstimatedTime = estimatedTime;
        }
    }
}