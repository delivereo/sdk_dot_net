﻿using Delivereo.SDK.Base;
using Delivereo.SDK.Detail.Response;
using Delivereo.SDK.Enums;
using Delivereo.SDK.Request.Calculate;
using Delivereo.SDK.Request.Cancel;
using Delivereo.SDK.Request.Create;
using Delivereo.SDK.Request.Login;
using Delivereo.SDK.Request.Rate;
using Delivereo.SDK.Request.Retry;
using Delivereo.SDK.Response.Calculate;
using Delivereo.SDK.Response.Create;
using Delivereo.SDK.Response.Login;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Delivereo.SDK.Client
{
    /// <summary>
    /// Business client
    /// </summary>
    public class BusinessClient
    {
        private readonly Language language;
        private static readonly HttpClient httpClient;

        static BusinessClient()
        {
            httpClient = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="serverEnvironment">Server environment to choose</param>
        /// <param name="language">Language of the requests</param>
        public BusinessClient(ServerEnvironment serverEnvironment, Language language)
        {
            this.language = language;

            // Update port # in the following line.
            httpClient.BaseAddress = new Uri(serverEnvironment.Value);
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Login to API
        /// </summary>
        /// <param name="apiKey">Business api key</param>
        /// <param name="businessEmail">Business email</param>
        /// <param name="businessRuc">Business ruc</param>
        /// <returns>Sign in response</returns>
        public SignInResponse Login(string apiKey, string businessEmail, string businessRuc)
        {
            var body = new SignIn(language, apiKey, businessEmail, businessRuc);
            return GetResultFluent<SignInResponse, SignIn>(BusinessConstants.LoginUrl, "", body, true);
        }

        /// <summary>
        /// Token renewal
        /// </summary>
        /// <param name="oldJwtToken">Old JWT Token</param>
        /// <returns></returns>
        public TokenRenewalResponse TokenRenewal(string oldJwtToken, string businessEmail)
        {
            var body = new TokenRenewal(language, businessEmail, oldJwtToken);
            return GetResultFluent<TokenRenewalResponse, TokenRenewal>(BusinessConstants.TokenRenewalUrl, "", body, true);
        }

        /// <summary>
        /// Calculate booking using geographic points
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="cityType">City</param>
        /// <param name="points">Geographic points</param>
        /// <returns>Calculate response</returns>
        public CalculateResponse CalculateBookingCostUsingPoints(string jwtToken, City cityType, List<CalculatePoint> points)
        {
            var body = new CalculateBooking(language, PackageSize.SMALL, cityType, points, new List<CalculateAddress>());
            return GetResultFluent<CalculateResponse, CalculateBooking>(BusinessConstants.CalculateUrl, jwtToken, body);
        }

        /// <summary>
        /// Calculate booking using addresses
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="cityType">City</param>
        /// <param name="addresses">Addresses</param>
        /// <returns>Calculate response</returns>
        public CalculateResponse CalculateBookingCostUsingAddresses(string jwtToken, City cityType, List<CalculateAddress> addresses)
        {
            var body = new CalculateBooking(language, PackageSize.SMALL, cityType, new List<CalculatePoint>(), addresses);
            return GetResultFluent<CalculateResponse, CalculateBooking>(BusinessConstants.CalculateUrl, jwtToken, body);
        }

        /// <summary>
        /// Create booking using points
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="cityType">City</param>
        /// <param name="points">Geographic points</param>
        /// <param name="description">Description of the booking</param>
        /// <param name="orderTotal">Total amount of the order including taxes</param>
        /// <param name="order">Order detail</param>
        /// <param name="shopName">Shop name (optional)(</param>
        /// <param name="shopIcon">Shop icon as url (optional)</param>
        /// <param name="bookingBusinessUserEmail">Booking owner business user email (optional)</param>
        /// <param name="extraInstructions">Extra instructions of the booking (optional)</param>
        /// <returns>Create response</returns>
        public CreateResponse CreateBookingUsingPoints(string jwtToken, City cityType, List<CreatePoint> points,
            string description, double orderTotal, CreateOrder order, string shopName = "", string shopIcon = "",
            string bookingBusinessUserEmail = "", string extraInstructions = "")
        {
            var body = new CreateBooking(language, PackageSize.SMALL, cityType, points, new List<CreateAddress>(), description,
                "", shopName, shopIcon, orderTotal, extraInstructions, null, order, bookingBusinessUserEmail);
            return GetResultFluent<CreateResponse, CreateBooking>(BusinessConstants.CreateUrl, jwtToken, body);
        }

        /// <summary>
        /// Create booking using addresses
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="cityType">City</param>
        /// <param name="addresses">Addresses</param>
        /// <param name="description">Description of the booking</param>
        /// <param name="orderTotal">Total amount of the order including taxes</param>
        /// <param name="order">Order detail</param>
        /// <param name="shopName">Shop name (optional)(</param>
        /// <param name="shopIcon">Shop icon as url (optional)</param>
        /// <param name="bookingBusinessUserEmail">Booking owner business user email (optional)</param>
        /// <param name="extraInstructions">Extra instructions of the booking (optional)</param>
        /// <returns>Create response</returns>
        public CreateResponse CreateBookingUsingAddresses(string jwtToken, City cityType, List<CreateAddress> addresses,
           string description, double orderTotal, CreateOrder order, string shopName = "", string shopIcon = "",
           string bookingBusinessUserEmail = "", string extraInstructions = "")
        {
            var body = new CreateBooking(language, PackageSize.SMALL, cityType, new List<CreatePoint>(), addresses, description,
                "", shopName, shopIcon, orderTotal, extraInstructions, null, order, bookingBusinessUserEmail);
            return GetResultFluent<CreateResponse, CreateBooking>(BusinessConstants.CreateUrl, jwtToken, body);
        }

        /// <summary>
        /// Create booking domicile/domicile pharmacy
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="businessFirstBranchName">Business first branch name</param>
        /// <param name="businessBranchNamesToTransfer">Business branches names to transfer</param>
        /// <param name="businessUserEmail">Business user email</param>
        /// <param name="orderReference">Order reference</param>
        /// <param name="clientPersonalId">Client personal id</param>
        /// <param name="clientFullName">Client full name</param>
        /// <param name="clientAddress">Client address</param>
        /// <param name="clientPaymentMode">Client payment mode</param>
        /// <param name="countryCode">Country code</param>
        /// <param name="clientMobileNumber">Client mobile number</param>
        /// <param name="clientReference">Client reference</param>
        /// <returns>Create response</returns>
        public CreateResponse CreateBookingDomicilePharmacy(string jwtToken, string businessFirstBranchName, List<string> businessBranchNamesToTransfer, string businessUserEmail, string orderReference, string clientPersonalId, string clientFullName, string clientAddress, PaymentMode clientPaymentMode, string countryCode, string clientMobileNumber = "", string clientReference = "")
        {
            var body = new CreateBookingDomicilePharmacy(language, businessFirstBranchName, businessBranchNamesToTransfer,
              businessUserEmail, orderReference, clientPersonalId, clientFullName, clientAddress, clientPaymentMode, countryCode, clientMobileNumber, clientReference);
            return GetResultFluent<CreateResponse, CreateBookingDomicilePharmacy>(BusinessConstants.CreateDomicilePharmacyUrl, jwtToken, body);
        }

        /// <summary>
        /// Cancel booking by id
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="bookingId">Booking id</param>
        /// <param name="cancelReason">Cancel reason</param>
        /// <param name="cancelFeedback">Cancel feedback</param>
        /// <returns>Basic response</returns>
        public BasicResponse CancelBookingById(string jwtToken, int bookingId, CancelReason cancelReason, string cancelFeedback)
        {
            var body = new CancelBooking(language, bookingId, cancelReason, cancelFeedback);
            return GetResultFluent<BasicResponse, CancelBooking>(BusinessConstants.CancelUrl, jwtToken, body);
        }

        /// <summary>
        /// Retry booking by id
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="bookingId">Booking id</param>
        /// <returns>Basic response</returns>
        public BasicResponse RetryBookingById(string jwtToken, int bookingId)
        {
            var body = new RetryBooking(language, bookingId);
            return GetResultFluent<BasicResponse, RetryBooking>(BusinessConstants.RetryUrl, jwtToken, body);
        }

        /// <summary>
        /// Rate driver by booking id
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="bookingId">Booking id</param>
        /// <param name="rating">Rating of the booking (1-5)</param>
        /// <param name="ratingLowReason">Rating low reason</param>
        /// <param name="ratingLowFeedback">Rating low feedback</param>
        /// <returns>Basic response</returns>
        public BasicResponse RateDriverForBookingById(string jwtToken, int bookingId, double rating, RatingLowReason ratingLowReason, string ratingLowFeedback)
        {
            var body = new RateDriver(language, bookingId, rating, ratingLowReason, ratingLowFeedback);
            return GetResultFluent<BasicResponse, RateDriver>(BusinessConstants.RateDriverUrl, jwtToken, body);
        }

        /// <summary>
        /// Get full booking detail by booking id
        /// </summary>
        /// <param name="jwtToken">Jwt token</param>
        /// <param name="bookingId">Booking id</param>
        /// <returns>Booking response</returns>
        public BookingResponse GetBookingFullDetailById(string jwtToken, int bookingId)
        {
            var body = new DetailBooking(language, bookingId);
            return GetResultFluent<BookingResponse, DetailBooking>(BusinessConstants.DetailUrl, jwtToken, body);
        }

        /// <summary>
        /// Get result from fluent request using flurl
        /// </summary>
        /// <typeparam name="T">Basic response</typeparam>
        /// <typeparam name="K">Basic request</typeparam>
        /// <param name="url">Url for the request</param>
        /// <param name="jwtToken">JWT token for the request</param>
        /// <param name="body">Body for the request></param>
        /// <param name="basicAuth">Use basic auth</param>
        /// <returns></returns>
        private T GetResultFluent<T, K>(string url, string jwtToken, K body, bool basicAuth = false) where T : BasicResponse where K : BasicRequest
        {
            try
            {
                T response = null;

                if (basicAuth)
                {
                    httpClient.CancelPendingRequests();
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "c2VydmljZXMubW9iaWxlQGRlbGl2ZXJlby5jb206ZGVsaXZlcmVvLyotK2FiYzEyMw==");
                    HttpResponseMessage result = httpClient.PostAsJsonAsync(url, body).Result;
                    result.EnsureSuccessStatusCode();

                    // Deserialize the updated product from the response body.
                    response = result.Content.ReadAsAsync<T>().Result;
                }
                else
                {
                    httpClient.CancelPendingRequests();
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwtToken);
                    HttpResponseMessage result = httpClient.PostAsJsonAsync(url, body).Result;
                    result.EnsureSuccessStatusCode();

                    // Deserialize the updated product from the response body.
                    response = result.Content.ReadAsAsync<T>().Result;
                }

                if (response != null)
                {
                    return response;
                }

                return null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}