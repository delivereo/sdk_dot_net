﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;

namespace Delivereo.SDK.Request.Login
{
    /// <summary>
    /// Sign in
    /// </summary>
    public class SignIn : BasicRequest
    {
        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("ruc")]
        public string Ruc { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SignIn()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="apiKey">Business api key</param>
        /// <param name="email">Business email</param>
        /// <param name="ruc">Business ruc</param>
        public SignIn(Language lang, string apiKey, string email, string ruc) : base(lang)
        {
            this.ApiKey = apiKey;
            this.Email = email;
            this.Ruc = ruc;
        }
    }
}