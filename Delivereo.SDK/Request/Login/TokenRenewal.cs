﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;

namespace Delivereo.SDK.Request.Login
{
    /// <summary>
    /// Token renewal
    /// </summary>
    public class TokenRenewal : BasicRequest
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("oldJwtToken")]
        public string OldJwtToken { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public TokenRenewal()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="email">Business email</param>
        /// <param name="oldJwtToken">Old JWT Token</param>
        public TokenRenewal(Language lang, string email, string oldJwtToken) : base(lang)
        {
            this.Email = email;
            this.OldJwtToken = oldJwtToken;
        }
    }
}