﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;

namespace Delivereo.SDK.Request.Rate
{
    /// <summary>
    /// Rate driver
    /// </summary>
    public class RateDriver : BasicRequest
    {
        [JsonProperty("bookingId")]
        public int BookingId { get; set; }

        [JsonProperty("rating")]
        public double Rating { get; set; }

        [JsonProperty("ratingLowReasonType")]
        public RatingLowReason RatingLowReasonType { get; set; }

        [JsonProperty("ratingLowFeedback")]
        public string RatingLowFeedback { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public RateDriver()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="bookingId">Booking id</param>
        /// <param name="rating">Rating of the booking (1-5)</param>
        /// <param name="ratingLowReasonType">Rating low reason</param>
        /// <param name="ratingLowFeedback">Rating low feedback</param>
        public RateDriver(Language lang, int bookingId, double rating, RatingLowReason ratingLowReasonType, string ratingLowFeedback) : base(lang)
        {
            this.BookingId = bookingId;
            this.Rating = rating;
            this.RatingLowReasonType = ratingLowReasonType;
            this.RatingLowFeedback = ratingLowFeedback;
        }
    }
}