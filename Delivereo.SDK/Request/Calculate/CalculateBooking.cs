﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;
using System.Collections.Generic;

namespace Delivereo.SDK.Request.Calculate
{
    /// <summary>
    /// Calculate booking
    /// </summary>
    public class CalculateBooking : BasicRequest
    {
        [JsonProperty("categoryType")]
        public PackageSize CategoryType { get; set; }

        [JsonProperty("cityType")]
        public City CityType { get; set; }

        [JsonProperty("points")]
        public List<CalculatePoint> Points { get; set; }

        [JsonProperty("addresses")]
        public List<CalculateAddress> Addresses { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CalculateBooking()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="categoryType">Package size</param>
        /// <param name="cityType">City of the booking</param>
        /// <param name="points">Points where to deliver</param>
        /// <param name="addresses">Addresses where to deliver (mutual exclusive field between points)</param>
        public CalculateBooking(Language lang, PackageSize categoryType, City cityType, List<CalculatePoint> points, List<CalculateAddress> addresses) : base(lang)
        {
            this.CategoryType = categoryType;
            this.CityType = cityType;
            this.Points = points;
            this.Addresses = addresses;
        }
    }
}