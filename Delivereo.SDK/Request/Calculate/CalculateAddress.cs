﻿
using Newtonsoft.Json;
using System;

namespace Delivereo.SDK.Request.Calculate
{
    /// <summary>
    /// Calculate address
    /// </summary>
    public class CalculateAddress
    {
        [JsonProperty("addressOrder")]
        public int AddressOrder { get; set; }

        [JsonProperty("fullAddress")]
        public string FullAddress { get; set; }

        [Obsolete("Use full address instead.")]
        [JsonProperty("addressMainStreet")]
        public string AddressMainStreet { get; set; }

        [Obsolete("Use full address instead.")]
        [JsonProperty("addressCrossingStreet")]
        public string AddressCrossingStreet { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CalculateAddress()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="addressOrder">Address order (Begins from 1-15)</param>
        /// <param name="fullAddress">Full address</param>
        /// <param name="addressMainStreet">Address main street (obsolete)</param>
        /// <param name="addressCrossingStreet">Address crossing street (obsolete)</param>
        /// <param name="countryCode">Country code based on ISO codes</param>
        public CalculateAddress(int addressOrder, string fullAddress, string addressMainStreet, string addressCrossingStreet, string countryCode)
        {
            this.AddressOrder = addressOrder;
            this.FullAddress = fullAddress;
            this.AddressMainStreet = addressMainStreet;
            this.AddressCrossingStreet = addressCrossingStreet;
            this.CountryCode = countryCode;
        }
    }
}