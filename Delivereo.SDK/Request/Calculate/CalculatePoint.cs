﻿
using Newtonsoft.Json;

namespace Delivereo.SDK.Request.Calculate
{
    /// <summary>
    /// Calculate point
    /// </summary>
    public class CalculatePoint
    {
        [JsonProperty("pointOrder")]
        public int PointOrder { get; set; }

        [JsonProperty("pointLatitude")]
        public double PointLatitude { get; set; }

        [JsonProperty("pointLongitude")]
        public double PointLongitude { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CalculatePoint()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="pointOrder">Point order (Begins from 1-15)</param>
        /// <param name="pointLatitude">Point latitude</param>
        /// <param name="pointLongitude">Point longitude</param>
        public CalculatePoint(int pointOrder, double pointLatitude, double pointLongitude)
        {
            this.PointOrder = pointOrder;
            this.PointLatitude = pointLatitude;
            this.PointLongitude = pointLongitude;
        }
    }
}