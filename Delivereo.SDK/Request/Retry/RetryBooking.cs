﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;

namespace Delivereo.SDK.Request.Retry
{
    /// <summary>
    /// Retry booking
    /// </summary>
    public class RetryBooking : BasicRequest
    {       
        [JsonProperty("bookingId")]
        public int BookingId { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public RetryBooking()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="bookingId">Booking id</param>
        public RetryBooking(Language lang, int bookingId) : base(lang)
        {
            this.BookingId = bookingId;
        }
    }
}