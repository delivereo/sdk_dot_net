﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;

namespace Delivereo.SDK.Request.Retry
{
    /// <summary>
    /// Detail booking
    /// </summary>
    public class DetailBooking : BasicRequest
    {
        [JsonProperty("bookingId")]
        public int BookingId { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public DetailBooking()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="bookingId">Booking id</param>
        public DetailBooking(Language lang, int bookingId) : base(lang)
        {
            this.BookingId = bookingId;
        }
    }
}