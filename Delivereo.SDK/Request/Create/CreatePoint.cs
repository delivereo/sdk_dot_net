﻿
using Newtonsoft.Json;

namespace Delivereo.SDK.Request.Create
{
    /// <summary>
    /// Create point
    /// </summary>
    public class CreatePoint
    {
        [JsonProperty("pointOrder")]
        public int PointOrder { get; set; }

        [JsonProperty("senderRecipientName")]
        public string SenderRecipientName { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }

        [JsonProperty("pointLatitude")]
        public double PointLatitude { get; set; }

        [JsonProperty("pointLongitude")]
        public double PointLongitude { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreatePoint()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="pointOrder">Point order (Begins from 1-15)</param>
        /// <param name="senderRecipientName">Sender or recipient name</param>
        /// <param name="address">Real full address</param>
        /// <param name="reference">Reference to the full address</param>        
        /// <param name="pointLatitude">Point latitude</param>
        /// <param name="pointLongitude">Point longitude</param>
        public CreatePoint(int pointOrder, string senderRecipientName, string address, string reference, double pointLatitude, double pointLongitude)
        {
            this.PointOrder = pointOrder;
            this.SenderRecipientName = senderRecipientName;
            this.Address = address;
            this.Reference = reference;
            this.PointLatitude = pointLatitude;
            this.PointLongitude = pointLongitude;
        }
    }
}