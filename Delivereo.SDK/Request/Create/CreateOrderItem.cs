﻿
using Newtonsoft.Json;

namespace Delivereo.SDK.Request.Create
{
    /// <summary>
    /// Create order item
    /// </summary>
    public class CreateOrderItem
    {
        [JsonProperty("orderItemGuid")]
        public string OrderItemGuid { get; set; }

        [JsonProperty("orderItemName")]
        public string OrderItemName { get; set; }

        [JsonProperty("orderItemDescription")]
        public string OrderItemDescription { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("unitSubTotal")]
        public double UnitSubTotal { get; set; }

        [JsonProperty("unitIva")]
        public double UnitIva { get; set; }

        [JsonProperty("unitTotal")]
        public double UnitTotal { get; set; }

        [JsonProperty("orderItemPriceTotal")]
        public double OrderItemPriceTotal { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateOrderItem()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="orderItemGuid">Order item id from the client</param>
        /// <param name="orderItemName">Order item name</param>
        /// <param name="orderItemDescription">Order item description (optional)</param>
        /// <param name="quantity">Quantity of items in the order</param>
        /// <param name="unitSubTotal">Unit price of the item without taxes</param>
        /// <param name="unitIva">Unit tax percentage</param>
        /// <param name="unitTotal">Total price of the items calculated by using unit sub total * quantity</param>
        /// <param name="orderItemPriceTotal">Total price of the items with taxes calculated by using unit total times (1 + unit iva)</param>
        public CreateOrderItem(string orderItemGuid, string orderItemName, string orderItemDescription, long quantity, double unitSubTotal, double unitIva, double unitTotal, double orderItemPriceTotal)
        {
            this.OrderItemGuid = orderItemGuid;
            this.OrderItemName = orderItemName;
            this.OrderItemDescription = orderItemDescription;
            this.Quantity = quantity;
            this.UnitSubTotal = unitSubTotal;
            this.UnitIva = unitIva;
            this.UnitTotal = unitTotal;
            this.OrderItemPriceTotal = orderItemPriceTotal;
        }
    }
}