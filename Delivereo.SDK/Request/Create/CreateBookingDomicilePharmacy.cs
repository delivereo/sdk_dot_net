﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;
using System.Collections.Generic;

namespace Delivereo.SDK.Request.Create
{
    /// <summary>
    /// Create booking
    /// </summary>
    public class CreateBookingDomicilePharmacy : BasicRequest
    {
        [JsonProperty("businessFirstBranchName")]
        public string BusinessFirstBranchName { get; set; }

        [JsonProperty("businessBranchNamesToTransfer")]
        public List<string> BusinessBranchNamesToTransfer { get; set; }

        [JsonProperty("businessUserEmail")]
        public string BusinessUserEmail { get; set; }

        [JsonProperty("orderReference")]
        public string OrderReference { get; set; }

        [JsonProperty("clientPersonalId")]
        public string ClientPersonalId { get; set; }

        [JsonProperty("clientFullName")]
        public string ClientFullName { get; set; }

        [JsonProperty("clientAddress")]
        public string ClientAddress { get; set; }

        [JsonProperty("clientPaymentMode")]
        public PaymentMode ClientPaymentMode { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("clientMobileNumber")]
        public string ClientMobileNumber { get; set; }

        [JsonProperty("clientReference")]
        public string ClientReference { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateBookingDomicilePharmacy()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="businessFirstBranchName">Business first branch name</param>
        /// <param name="businessBranchNamesToTransfer">Business branches names to transfer</param>
        /// <param name="businessUserEmail">Business user email</param>
        /// <param name="orderReference">Order reference</param>
        /// <param name="clientPersonalId">Client personal id</param>
        /// <param name="clientFullName">Client full name</param>
        /// <param name="clientAddress">Client address</param>
        /// <param name="clientPaymentMode">Client payment mode</param>
        /// <param name="countryCode">Country code</param>
        /// <param name="clientMobileNumber">Client mobile number</param>
        /// <param name="clientReference">Client reference</param>
        public CreateBookingDomicilePharmacy(Language lang, string businessFirstBranchName, List<string> businessBranchNamesToTransfer, string businessUserEmail, string orderReference, string clientPersonalId, string clientFullName, string clientAddress, PaymentMode clientPaymentMode, string countryCode, string clientMobileNumber, string clientReference) : base(lang)
        {
            this.BusinessFirstBranchName = businessFirstBranchName;
            this.BusinessBranchNamesToTransfer = businessBranchNamesToTransfer;
            this.BusinessUserEmail = businessUserEmail;
            this.OrderReference = orderReference;
            this.ClientPersonalId = clientPersonalId;
            this.ClientFullName = clientFullName;
            this.ClientAddress = clientAddress;
            this.ClientPaymentMode = clientPaymentMode;
            this.CountryCode = countryCode;
            this.ClientMobileNumber = clientMobileNumber;
            this.ClientReference = clientReference;
        }
    }
}