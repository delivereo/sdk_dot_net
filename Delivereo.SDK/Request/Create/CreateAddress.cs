﻿
using Newtonsoft.Json;
using System;

namespace Delivereo.SDK.Request.Create
{
    /// <summary>
    /// Create address
    /// </summary>
    public class CreateAddress
    {
        [JsonProperty("addressOrder")]
        public int AddressOrder { get; set; }

        [JsonProperty("fullAddress")]
        public string FullAddress { get; set; }

        [Obsolete("Use full address instead.")]
        [JsonProperty("addressMainStreet")]
        public string AddressMainStreet { get; set; }

        [Obsolete("Use full address instead.")]
        [JsonProperty("addressCrossingStreet")]
        public string AddressCrossingStreet { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("senderRecipientName")]
        public string SenderRecipientName { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateAddress()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="addressOrder">Address order (Begins from 1 to 15)</param>
        /// <param name="fullAddress">Full address</param>
        /// <param name="addressMainStreet">Address main street (obsolete)</param>
        /// <param name="addressCrossingStreet">Address crossing street (obsolete)</param>
        /// <param name="countryCode">Country code based on ISO codes</param>
        /// <param name="senderRecipientName">Sender or recipient name</param>
        /// <param name="address">Real full address</param>
        /// <param name="reference">Reference to the full address</param>
        public CreateAddress(int addressOrder, string fullAddress, string addressMainStreet, string addressCrossingStreet, string countryCode, string senderRecipientName, string address, string reference)
        {
            this.AddressOrder = addressOrder;
            this.FullAddress = fullAddress;
            this.AddressMainStreet = addressMainStreet;
            this.AddressCrossingStreet = addressCrossingStreet;
            this.CountryCode = countryCode;
            this.SenderRecipientName = senderRecipientName;
            this.Address = address;
            this.Reference = reference;
        }
    }
}