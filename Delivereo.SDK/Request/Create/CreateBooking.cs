﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;
using System;
using System.Collections.Generic;

namespace Delivereo.SDK.Request.Create
{
    /// <summary>
    /// Create booking
    /// </summary>
    public class CreateBooking : BasicRequest
    {
        [JsonProperty("categoryType")]
        public PackageSize CategoryType { get; set; }

        [JsonProperty("cityType")]
        public City CityType { get; set; }

        [JsonProperty("points")]
        public List<CreatePoint> Points { get; set; }

        [JsonProperty("addresses")]
        public List<CreateAddress> Addresses { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("bookingImage")]
        public string BookingImage { get; set; }

        [JsonProperty("shopName")]
        public string ShopName { get; set; }

        [JsonProperty("shopIcon")]
        public string ShopIcon { get; set; }

        [JsonProperty("itemsPrice")]
        public double ItemsPrice { get; set; }

        [JsonProperty("extraInstructions")]
        public string ExtraInstructions { get; set; }

        [JsonProperty("maxSuggestedTime")]
        public DateTime? MaxSuggestedTime { get; set; }

        [JsonProperty("order")]
        public CreateOrder Order { get; set; }

        [JsonProperty("bookingBusinessUserEmail")]
        public string BookingBusinessUserEmail { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateBooking()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="categoryType">Package size</param>
        /// <param name="cityType">City of the booking</param>
        /// <param name="points">Points where to deliver</param>
        /// <param name="addresses">Addresses where to deliver (mutual exclusive field between points)</param>
        /// <param name="description">Description of the booking</param>
        /// <param name="bookingImage">Booking image as base 64</param>
        /// <param name="shopName">Shop name where the booking comes from</param>
        /// <param name="shopIcon">Shop icon as url</param>
        /// <param name="itemsPrice">Total amount of the booking</param>
        /// <param name="extraInstructions">Extra instructions of the booking</param>
        /// <param name="maxSuggestedTime">Max suggested time to apply to the booking</param>
        /// <param name="order">Order detail</param>
        /// <param name="bookingBusinessUserEmail">Booking owner business user email</param>
        public CreateBooking(Language lang, PackageSize categoryType, City cityType, List<CreatePoint> points, List<CreateAddress> addresses, string description, string bookingImage, string shopName, string shopIcon, double itemsPrice, string extraInstructions, DateTime? maxSuggestedTime, CreateOrder order, string bookingBusinessUserEmail) : base(lang)
        {
            this.CategoryType = categoryType;
            this.CityType = cityType;
            this.Points = points;
            this.Addresses = addresses;
            this.Description = description;
            this.BookingImage = bookingImage;
            this.ShopName = shopName;
            this.ShopIcon = shopIcon;
            this.ItemsPrice = itemsPrice;
            this.ExtraInstructions = extraInstructions;
            this.MaxSuggestedTime = maxSuggestedTime;
            this.Order = order;
            this.BookingBusinessUserEmail = bookingBusinessUserEmail;
        }
    }
}