﻿using Newtonsoft.Json;
using Delivereo.SDK.Enums;
using System.Collections.Generic;

namespace Delivereo.SDK.Request.Create
{
    /// <summary>
    /// Create order
    /// </summary>
    public class CreateOrder
    {
        [JsonProperty("orderGuid")]
        public string OrderGuid { get; set; }

        [JsonProperty("orderSubTotal")]
        public double OrderSubTotal { get; set; }

        [JsonProperty("paymentMode")]
        public PaymentMode PaymentMode { get; set; }

        [JsonProperty("orderIva")]
        public double OrderIva { get; set; }

        [JsonProperty("orderTotal")]
        public double OrderTotal { get; set; }

        [JsonProperty("orderItems")]
        public List<CreateOrderItem> OrderItems { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateOrder()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="orderGuid">Order id reference from the client, same value as order reference</param>
        /// <param name="orderSubTotal">Order subtotal, not including taxes</param>
        /// <param name="paymentMode">Payment mode of the final client</param>
        /// <param name="orderIva">Order tax value</param>
        /// <param name="orderTotal">Order total which is equals to order sub total + order iva</param>
        /// <param name="orderItems">Detail of the items in the order</param>
        public CreateOrder(string orderGuid, double orderSubTotal, PaymentMode paymentMode, double orderIva, double orderTotal, List<CreateOrderItem> orderItems)
        {
            this.OrderGuid = orderGuid;
            this.OrderSubTotal = orderSubTotal;
            this.PaymentMode = paymentMode;
            this.OrderIva = orderIva;
            this.OrderTotal = orderTotal;
            this.OrderItems = orderItems;
        }
    }
}