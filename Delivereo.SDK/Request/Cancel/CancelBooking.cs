﻿using Newtonsoft.Json;
using Delivereo.SDK.Base;
using Delivereo.SDK.Enums;

namespace Delivereo.SDK.Request.Cancel
{
    /// <summary>
    /// Cancel booking
    /// </summary>
    public class CancelBooking : BasicRequest
    {
        [JsonProperty("bookingId")]
        public int BookingId { get; set; }

        [JsonProperty("cancelReason")]
        public CancelReason CancelReason { get; set; }

        [JsonProperty("cancelFeedback")]
        public string CancelFeedback { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CancelBooking()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        /// <param name="bookingId">Booking id</param>
        /// <param name="cancelReason">Cancel reason</param>
        /// <param name="cancelFeedback">Cancel feedback</param>
        public CancelBooking(Language lang, int bookingId, CancelReason cancelReason, string cancelFeedback) : base(lang)
        {
            this.BookingId = bookingId;
            this.CancelReason = cancelReason;
            this.CancelFeedback = cancelFeedback;
        }
    }
}