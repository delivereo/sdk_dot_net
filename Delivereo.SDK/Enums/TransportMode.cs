﻿
namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Transport mode
    /// </summary>
    public enum TransportMode
    {
        NONE,
        CAR,
        MOTORCYCLE,
        BICYCLE,
        WALK
    }
}