﻿namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Server environment
    /// </summary>
    public class ServerEnvironment
    {
        /// <summary>
        /// Server environment constructor
        /// </summary>
        /// <param name="value">Server environment value</param>
        private ServerEnvironment(string value) { Value = value; }

        public string Value { get; set; }

        public static ServerEnvironment Development { get { return new ServerEnvironment("http://dgamc.ddns.net:8080"); } }

        public static ServerEnvironment Test { get { return new ServerEnvironment("http://delivereo-test-advanced.us-east-1.elasticbeanstalk.com"); } }

        public static ServerEnvironment Staging { get { return new ServerEnvironment("https://delivereo-test.com"); } }

        public static ServerEnvironment Production { get { return new ServerEnvironment("https://delivereo.com"); } }
    }
}
