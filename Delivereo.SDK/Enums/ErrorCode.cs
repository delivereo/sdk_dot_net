﻿
namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Error code
    /// </summary>
    public enum ErrorCode
    {
        NOT_ENOUGH_DATA = 401,
        INCORRECT_DATA = 402,
        DATA_NOT_FOUND = 404,
        FAILURE = 405,
        SUCCESS = 200,
        INCORRECT_CREDENTIALS = 204,
        USER_MOBILE_NUMBER_EMPTY = 205,
        USER_SUSPENDED = 206,
        USER_DISABLED = 207,
        USER_NOT_VERIFIED = 208
    }
}