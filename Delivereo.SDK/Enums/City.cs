﻿
namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// City
    /// </summary>
    public enum City
    {
        QUITO,
        GUAYAQUIL,
        SANGOLQUI,
        RIOBAMBA,
        AMBATO,
        BABAHOYO,
        IBARRA,
        CUENCA,
        MANTA,
        PORTOVIEJO,
        SANTO_DOMINGO,
        LATACUNGA,
        ESMERALDAS,
        QUININDE,
        PEDERNALES,
        LA_CONCORDIA,
        OTAVALO;
    }
}