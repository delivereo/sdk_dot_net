﻿
namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Booking status
    /// </summary>
    public enum BookingStatus
    {
        MANUAL_ASSIGN = -4,
        TRANSFERRING_TO = -3,
        NO_DRIVER_FOUND = -2,
        CANCELED = -1,
        CREATED = 0,
        GOING_FIRST_POSITION = 1,
        ARRIVED_FIRST_POSITION = 2,
        BUYING_ITEM = 3,
        GOING_OTHER_POSITION = 4,
        ARRIVED_OTHER_POSITION = 5,
        GOING_LAST_POSITION = 6,
        ARRIVED_LAST_POSITION = 7,
        CHARGE = 8,
        RATING = 9,
        FINISHED = 10
    }
}