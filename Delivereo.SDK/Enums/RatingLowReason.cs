﻿
namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Rating low reason
    /// </summary>
    public enum RatingLowReason
    {
        TOO_SLOW,
        BAD_APPEARANCE,
        BAD_BEHAVIOR,
        PACKAGE_IN_BAD_SHAPE,
        OTHER,
        NONE
    }
}
