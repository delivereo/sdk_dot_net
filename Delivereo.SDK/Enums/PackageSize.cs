﻿
namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Package size
    /// </summary>
    public enum PackageSize
    {
        SMALL,
        MEDIUM,
        LARGE
    }
}