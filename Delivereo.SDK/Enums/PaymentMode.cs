﻿
namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Payment mode
    /// </summary>
    public enum PaymentMode
    {
        CASH,
        CREDIT_CARD,
        WALLET,
        NONE,
        CHECK,
        AGREEMENT,
        MIXED
    }
}