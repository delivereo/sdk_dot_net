﻿
namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Cancel reason
    /// </summary>
    public enum CancelReason
    {
        NOT_NEEDED_ANYMORE,
        TOO_MUCH_DELAY,
        BETTER_OPTION,
        OTHER,
    }
}