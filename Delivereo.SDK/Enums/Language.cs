﻿namespace Delivereo.SDK.Enums
{
    /// <summary>
    /// Language
    /// </summary>
    public class Language
    {
        /// <summary>
        /// Language constructor
        /// </summary>
        /// <param name="value">Language value</param>
        private Language(string value) { Value = value; }

        public string Value { get; set; }

        public static Language Spanish { get { return new Language("es"); } }

        public static Language English { get { return new Language("en"); } }
    }
}
