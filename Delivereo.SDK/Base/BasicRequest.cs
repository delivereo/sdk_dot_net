﻿
using Newtonsoft.Json;
using Delivereo.SDK.Enums;

namespace Delivereo.SDK.Base
{
    /// <summary>
    /// Basic request
    /// </summary>
    public class BasicRequest
    {
        [JsonProperty("lang")]
        public string Lang { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public BasicRequest()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic contructor
        /// </summary>
        /// <param name="lang">Language of the request</param>
        public BasicRequest(Language lang)
        {
            this.Lang = lang.Value;
        }
    }
}