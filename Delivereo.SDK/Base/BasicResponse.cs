﻿
namespace Delivereo.SDK.Base
{
    /// <summary>
    /// Basic response
    /// </summary>
    public class BasicResponse
    {
        public string Title { get; set; }

        public string Message { get; set; }

        public bool Status { get; set; }

        public int Code { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public BasicResponse()
        {
            // Empty constructor
        }

        /// <summary>
        /// Basic response
        /// </summary>
        /// <param name="title">Title of the response</param>
        /// <param name="message">Message of the response</param>
        /// <param name="status">Status of the response</param>
        /// <param name="code">Code of the response</param>
        public BasicResponse(string title, string message, bool status, int code)
        {
            this.Title = title;
            this.Message = message;
            this.Status = status;
            this.Code = code;
        }
    }
}