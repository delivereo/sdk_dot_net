﻿namespace Delivereo.SDK.Client
{
    /// <summary>
    /// Business constants, URLs to API
    /// </summary>
    public static class BusinessConstants
    {
        public static string LoginUrl = "/api/protected/login/business";

        public static string TokenRenewalUrl = "/api/protected/token-renewal/business";

        public static string ValidateAddressUrl = "/api/private/business-bookings/validate-address";

        public static string CalculateUrl = "/api/private/business-bookings/calculate";

        public static string CreateUrl = "/api/private/business-bookings/create";

        public static string CreateDomicilePharmacyUrl = "/api/private/business-bookings/create-domicile";

        public static string CancelUrl = "/api/private/business-bookings/cancel";

        public static string RetryUrl = "/api/private/business-bookings/retry";

        public static string RateDriverUrl = "/api/private/business-bookings/rate-driver";

        public static string DetailUrl = "/api/private/business-bookings/detail-full";
    }
}
