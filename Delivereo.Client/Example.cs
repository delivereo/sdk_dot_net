﻿using Delivereo.SDK.Base;
using Delivereo.SDK.Client;
using Delivereo.SDK.Enums;
using Delivereo.SDK.Request.Calculate;
using Delivereo.SDK.Request.Create;
using Delivereo.SDK.Response.Create;
using System;
using System.Collections.Generic;

namespace Delivereo.Client
{
    public class Example
    {
        static void Start(string[] args)
        {
            try
            {
                // Crear una instancia del cliente de business. En server environment deberia apuntar a TEST
                var businessClient = new BusinessClient(ServerEnvironment.Test, Language.Spanish);

                // Logearse con las credenciales,
                var loginResponse = businessClient.Login("69982032-DD5", "info@delivereo.com", "1792715083001");

                // Si el login es exitoso, podemos usar el JWT token de la respuesta de login para poder ejecutar cualquier otra
                // accion en el API
                if (loginResponse.Status)
                {
                    // TOMAR EN CUENTA: Si el JWT token expira (al mandar una peticion que no sea login o renew token te devuelve codigo 403)
                    // Se puede generar un nuevo token volviendose a logear o usando el metodo siguiente
                    var renewTokenResponse = businessClient.TokenRenewal(loginResponse.JwtToken, "info@delivereo.com");

                    if (renewTokenResponse.Status)
                    {
                        Console.WriteLine("Nuevo jwt Token: " + renewTokenResponse.Result);
                        loginResponse.JwtToken = renewTokenResponse.Result;
                    }
                    else
                    {
                        Console.WriteLine("Nuevo JWT Token status: " + renewTokenResponse.Message);
                    }

                    // Calcular el costo de una carrera usando puntos geograficos
                    var points = new List<CalculatePoint>
                    {
                        new CalculatePoint(1, -0.19045013, -78.48059853),
                        new CalculatePoint(2, -0.172213, -78.486467),
                        new CalculatePoint(3, -0.20963146, -78.43948563)
                    };

                    var calculateResponse = businessClient.CalculateBookingCostUsingPoints(loginResponse.JwtToken, City.QUITO, points);

                    if (calculateResponse.Status)
                    {
                        Console.WriteLine("Costo con puntos geograficos: " + calculateResponse.TotalAmount);
                    }

                    // Calcular el costo de una carrera usando direcciones
                    var addresses = new List<CalculateAddress>()
                    {
                        new CalculateAddress()
                        {
                            AddressOrder = 1,
                            FullAddress = "Amazonas y Veintimilla",
                            CountryCode = "EC"
                        },
                          new CalculateAddress()
                        {
                            AddressOrder = 2,
                            FullAddress = "Francisco Dalmau y Calle 1",
                            CountryCode = "EC"
                        }
                    };

                    var calculateResponseAddress = businessClient.CalculateBookingCostUsingAddresses(loginResponse.JwtToken, City.QUITO, addresses);
                    if (calculateResponse.Status)
                    {
                        Console.WriteLine("Costo con direcciones: " + calculateResponseAddress.TotalAmount);
                    }

                    // No es necesario calcular el costo de una carrera si se va a crear una. Es solo referencial
                    // De igual manera se puede crear una carrera usando puntos geograficos o direcciones, la mejor opcion es
                    // usar direcciones por facilidad

                    // Crear carrera usando puntos geograficos
                    var pointsCreate = new List<CreatePoint>
                    {
                        new CreatePoint(1, "Jorge Cardenas", "Amazonas y Veintimilla E32A-32", "Edificio Tez Apartamento 200", -0.19045013, -78.48059853),
                        new CreatePoint(2, "Marcelo Bonilla", "Diego de Vazquez y Bellavista E34-23", "Condominio Carmen 2 casa 34", -0.172213, -78.486467),
                        new CreatePoint(3, "Pablo Mancero", "Naciones Unidas e Inaquito Esquina", "Edificio Metropolitan Oficina 409", -0.20963146, -78.43948563)
                    };

                    CreateOrder createOrder = new CreateOrder()
                    {
                        OrderGuid = "123456",
                        OrderItems = new List<CreateOrderItem>(),
                        OrderSubTotal = 10,
                        OrderIva = 1.2,
                        OrderTotal = 11.2,
                        PaymentMode = PaymentMode.CREDIT_CARD
                    };

                    var createResponse = businessClient.CreateBookingUsingPoints(loginResponse.JwtToken, City.QUITO, pointsCreate,
                            "Compra hamburguesa de pollo y hamburguesa de carne", 11.2, createOrder,
                            bookingBusinessUserEmail: "info@delivereo.com");

                    if (createResponse.Status)
                    {
                        Console.WriteLine("Nuevo Booking id con puntos geograficos: " + createResponse.BookingId);
                    }

                    var createAddresses = new List<CreateAddress>
                    {
                        new CreateAddress()
                        {
                            AddressOrder = 1,
                            FullAddress = "Amazonas y Veintimilla",
                            CountryCode = "EC",
                            SenderRecipientName = "Jorge Cardenas",
                            Address = "Amazonas y Veintimilla E32A-32",
                            Reference = "Edificio Tez Apartamento 200"
                        },
                          new CreateAddress()
                        {
                            AddressOrder = 2,
                            FullAddress = "Francisco Dalmau y Calle 1",
                            CountryCode = "EC",
                            SenderRecipientName = "Daniel Mancero",
                            Address = "Francisco Dalmau y Calle 1 OE32",
                            Reference = "Frente al Supermaxi"
                        }
                    };

                    CreateOrder createAdressOrder = new CreateOrder()
                    {
                        OrderGuid = "123456",
                        OrderItems = new List<CreateOrderItem>(),
                        OrderSubTotal = 10,
                        OrderIva = 1.2,
                        OrderTotal = 11.2,
                        PaymentMode = PaymentMode.CREDIT_CARD
                    };

                    CreateResponse createAddressResponse = businessClient.CreateBookingUsingAddresses(loginResponse.JwtToken, City.QUITO, createAddresses,
                            "Compra hamburguesa de pollo y hamburguesa de carne", 11.2, createAdressOrder,
                            bookingBusinessUserEmail: "info@delivereo.com");

                    if (createAddressResponse.Status)
                    {
                        Console.WriteLine("Nuevo Booking id con direcciones: " + createAddressResponse.BookingId);
                    }

                    // Crear carrera de tipo booking domicile
                    CreateResponse createBookingDomicileResponse = businessClient.CreateBookingDomicilePharmacy(loginResponse.JwtToken, "CCI Quito",
                            new List<string>(), "stefano@delivereo.com", "12345", "1715245933", "Daniel Mancero",
                            "Amazonas y naciones unidas", PaymentMode.CREDIT_CARD, "EC");

                    if (createBookingDomicileResponse.Status)
                    {
                        Console.WriteLine("Nuevo Booking id domicile: " + createBookingDomicileResponse.BookingId);
                    }

                    // Crear carrer de tipo booking domicile pharmacy
                    CreateResponse createBookingDomicilePharmacyResponse = businessClient.CreateBookingDomicilePharmacy(loginResponse.JwtToken, "CCI Quito",
                            new List<string> { "Inca Quito" }, "stefano@delivereo.com", "12345", "1715245933", "Daniel Mancero",
                            "Amazonas y naciones unidas", PaymentMode.CREDIT_CARD, "EC", "+593-983273919", "Departamento 5");

                    if (createBookingDomicilePharmacyResponse.Status)
                    {
                        Console.WriteLine("Nuevo Booking id domicile pharmacy: " + createBookingDomicilePharmacyResponse.BookingId);
                    }

                    // Para obtener el detalle de una carrera se puede utilizar el siguiente metodo
                    var bookingResponse = businessClient.GetBookingFullDetailById(
                            loginResponse.JwtToken,
                            createAddressResponse.BookingId);

                    if (bookingResponse.Status)
                    {
                        Console.WriteLine("Detalle Booking con direccion - descripcion: " + bookingResponse.Description);
                    }

                    // Si una carrera no puede ser atendida por alguno de nuestro delivererers, se puede reintentar
                    // usando el siguiente metodo
                    BasicResponse retryResponse = businessClient.RetryBookingById(
                            loginResponse.JwtToken,
                            (int)createAddressResponse.BookingId);

                    Console.WriteLine("Reintentar Booking con direccion, estado: " + retryResponse.Message);

                    // Despues de que la carrera fue atendida por un deliverer, esta puede ser calificada usando el
                    // siguiente metodo. No es obligatorio el calificar el conductor, pero se recomienda hacerlo si es
                    // posible. En una nueva version del API se va a generar una calificacion automatica, quitando esta
                    // opcion del API
                    BasicResponse rateDriverResponse = businessClient.RateDriverForBookingById(
                            loginResponse.JwtToken,
                            (int)createAddressResponse.BookingId,
                            5.0,
                            RatingLowReason.NONE,
                            "Everything OK");

                    Console.WriteLine("Calificar Conductor Booking con direccion, estado: " + rateDriverResponse.Message);

                    // Para cancelar una carrera, se puede utilizar el siguiente metodo
                    BasicResponse cancelResponse = businessClient.CancelBookingById(
                            loginResponse.JwtToken,
                            (int)createResponse.BookingId,
                            CancelReason.NOT_NEEDED_ANYMORE,
                            "Another option was found");

                    Console.WriteLine("Cancelar Booking con puntos geograficos, estado: " + cancelResponse.Message);

                    BasicResponse cancelAddressResponse = businessClient.CancelBookingById(
                            loginResponse.JwtToken,
                            (int)createAddressResponse.BookingId,
                            CancelReason.NOT_NEEDED_ANYMORE,
                            "Another option was found");

                    Console.WriteLine("Cancelar Booking con direccion, estado: " + cancelAddressResponse.Message);
                }
                else
                {
                    // Hubo problemas al logearse no podemos hacer nada mas
                    Console.WriteLine(loginResponse.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }
    }
}